package com.zonky.demo.config;

import com.zonky.demo.service.LoanService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public LoanService loanServiceTest() {
        return new LoanService();
    }

}
