package com.zonky.demo.service;

import com.zonky.demo.config.BeanConfiguration;
import com.zonky.demo.model.Loan;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RunWith(SpringRunner.class)
@Import(BeanConfiguration.class)
public class LoadServiceTestIT {

    @Autowired
    private LoanService loanServiceTest;

    @Test
    public void loadLoans() {
        loanServiceTest.updateLoans();
        List<Loan> loans = loanServiceTest.getLoans();
        Assert.assertFalse("Empty loan collection", CollectionUtils.isEmpty(loans));
    }

}