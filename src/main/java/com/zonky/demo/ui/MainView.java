package com.zonky.demo.ui;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.zonky.demo.model.Loan;
import com.zonky.demo.service.LoanService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Push
@Route("loans")
@SpringComponent
public class MainView extends VerticalLayout {

    private final LoanService loanService;
    private Grid<Loan> grid;

    @Autowired
    public MainView(@NonNull final LoanService loanService) {
        this.loanService = loanService;
        initComponents();
    }

    private void initComponents() {
        initGrid();
        initButton();
    }

    private void initButton() {
        Button reload = new Button("Refresh");
        reload.addClickListener((ComponentEventListener<ClickEvent<Button>>) buttonClickEvent -> {
            reloadGrid();
        });

        add(reload);
    }

    private void initGrid() {
        grid = new Grid<>(Loan.class);
        reloadGrid();
        add(grid);
    }

    private void reloadGrid() {
        grid.setItems(loanService.getLoans());
    }

}
