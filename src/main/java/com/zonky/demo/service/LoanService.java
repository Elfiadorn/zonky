package com.zonky.demo.service;

import com.zonky.demo.model.Loan;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class LoanService implements Serializable {


    private static final String MARKETPLACE_URL = "https://api.zonky.cz/loans/marketplace?datePublished__gt={date}";
    private static final RestTemplate restTemplate = new RestTemplate();
    private static final List<Loan> loans = Collections.synchronizedList(new ArrayList<>());
    private String lastUpdate = "";

    @NonNull
    public List<Loan> getLoans() {
        synchronized (loans) {
            return Collections.unmodifiableList(loans);
        }
    }

    @NonNull
    private Map<String, String> buildFilter() {
        Map<String, String> uriVariables = new HashMap<>();

        if (StringUtils.isEmpty(lastUpdate)) {
            uriVariables.put("date", "");
        } else {
            //TODO quick fix - restTemplate has issue with + symbol
            String[] split = lastUpdate.split("\\+");
            uriVariables.put("date", split[0]);
        }

        return uriVariables;
    }

    @Scheduled(fixedRate = 300000)
    void updateLoans() {
        Map<String, String> params = buildFilter();

        try {
            ResponseEntity<Loan[]> entities = restTemplate.getForEntity(MARKETPLACE_URL, Loan[].class, params);

            synchronized (loans) {
                loans.clear();

                if (Objects.requireNonNull(entities.getBody()).length > 0) {
                    log.info("Updating loans with {} records", entities.getBody().length);
                    updateLastUpdate(entities.getBody()[0]);
                    loans.addAll(Arrays.asList(entities.getBody()));
                } else {
                    log.info("No loans loaded");
                }
            }
        } catch (RestClientException rce) {
            log.error("Calling marketplace end with exception: {}", rce.getLocalizedMessage());
        }
    }

    private void updateLastUpdate(@NonNull final Loan loan) {
        lastUpdate = loan.getDatePublished();
    }

}
