# ZONKY DEMO APPLICATION

### Used technologies:
Java - backend
Spring - backend
Vaadin - frontend

### Build
mvn clean install

### Run
mvn spring-boot:run

### Browser
http://localhost:8080/loans